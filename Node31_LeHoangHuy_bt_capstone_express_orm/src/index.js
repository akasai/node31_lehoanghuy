import express from 'express';
import cors from "cors";
import rootRouter from "./routers/rootRouter.js"


const app = express();

app.use(express.json());
app.use(express.static(".")) 
app.use(cors());

app.listen(8000);
app.use("/api",rootRouter);
