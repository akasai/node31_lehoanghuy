//QUẢN LÝ RESTFUL API
import express from "express";
import { 
    userLogin, 
    userSignUp,
    addComment,
    uploadImage,
    saveImage,
    getUser,
    getListCreatedImage,
    getListSavedImage,
    removeCreatedImage,
    removeSavedImage,
    updateProfile
} 
from "../controllers/userController.js";
import { verifyToken } from "../config/jwt.js";
import {upload,uploadAvatar} from "../controllers/uploadController.js";

const userRouter = express.Router();

//localhost:8000/api/user/upload-image
userRouter.post("/upload-image",verifyToken,upload.single("file"),uploadImage)

//login
userRouter.post("/login",userLogin);

//sign up
userRouter.post("/signup",userSignUp);

//user comment
userRouter.post("/comment",verifyToken,addComment);

//user save image
userRouter.post("/save-image/:hinh_id",verifyToken,saveImage);

//user info
userRouter.get("/get-user",verifyToken,getUser);

//user get created image
userRouter.get("/get-created-img",verifyToken,getListCreatedImage);

//user get saved image
userRouter.get("/get-saved-img",verifyToken,getListSavedImage);

//user remove created image
userRouter.delete("/remove-created-img/:hinh_id",verifyToken,removeCreatedImage);

//user remove saved image
userRouter.delete("/remove-saved-img/:hinh_id",verifyToken,removeSavedImage);

//user update profile
userRouter.post("/update-profile",verifyToken,uploadAvatar.single("file"),updateProfile)

export default userRouter;