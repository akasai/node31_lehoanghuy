import express from "express";
import {getImg,getImgByName,getImgInfoByID,getImgCommentByID,checkSavedImgByID} from "../controllers/imgController.js"
import { verifyToken } from "../config/jwt.js";
const imgRouter = express.Router();

//get all image
imgRouter.get("/get-img",getImg);

//search image by image name
imgRouter.get("/search-img/:ten_hinh",getImgByName);

//get img info and user by image id
imgRouter.get("/get-img-info/:hinh_id",getImgInfoByID);

//get img comment and user by image id
imgRouter.get("/get-img-comment/:hinh_id",getImgCommentByID);

//check saved img by image id
imgRouter.get("/check-img/:hinh_id",verifyToken,checkSavedImgByID);


export default imgRouter;