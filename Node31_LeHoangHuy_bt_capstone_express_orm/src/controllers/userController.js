import {successCode,failCode,errorCode} from "../config/Response.js";
import fs from "fs";
import { PrismaClient } from "@prisma/client";
//yarn add bcrypt
import bcrypt from "bcrypt";
import {generateToken, checkToken, decodeToken} from "../config/jwt.js"

const prisma = new PrismaClient;

const userSignUp =async (req,res)=>{
    //them du lieu => create
    let {email,mat_khau,ho_ten,tuoi}=req.body;

    let newUser ={
        email,
        mat_khau: bcrypt.hashSync(mat_khau,10),
        ho_ten,
        tuoi : Number(tuoi)
    }

    //check email trung
    let checkEmail = await prisma.nguoi_dung.findMany({
        where:{
            email
        }
    })
    if(checkEmail.length>0){
        failCode(res,email,"Email already exist")
    }else{
        await prisma.nguoi_dung.create({data:newUser});
        successCode(res,email,"Successfully registered")
    }
    
}

const userLogin =async(req,res)=>{
    //tim kiem => Read
    let{email,mat_khau}=req.body;
    let checkUser = await prisma.nguoi_dung.findFirst({
        where:{
            email
        }
    })
    if(checkUser){
        if(bcrypt.compareSync(mat_khau,checkUser.mat_khau)){
            checkUser ={...checkUser,mat_khau:""};
            let token = generateToken(checkUser);
            successCode(res,token,"Successfully login");
        }else{
            failCode(res,mat_khau,"Wrong Password,pls check it");
        }  
    }else{
        failCode(res,email,"Email khong dung");
    }
    
}

const addComment =async (req,res)=>{
    try{
        let {hinh_id,noi_dung} = req.body;
        let {token} = req.headers
        let userInfo = await getInfoUser(token);
        let dataUser = await prisma.nguoi_dung.findFirst({
            where:{
                nguoi_dung_id :userInfo.nguoi_dung_id
            },
        });
     
        if(dataUser){
            let newComment ={
                nguoi_dung_id: Number(dataUser.nguoi_dung_id),
                hinh_id : Number(hinh_id),
                noi_dung,
                ngay_binh_luan: new Date() 
            }
            
            if(noi_dung || noi_dung !=""){
                await prisma.binh_luan.create({data:newComment});
                successCode(res,newComment,"Successfully comment on Picture") 
            }else{
                failCode(res,noi_dung,"Pls enter the comment")
            }
        }else{
            failCode(res,dataUser,"User not found");
        }
       
        
    }catch(err){
        console.log(err);
        failCode(res,"",err.message);
    }
    


};

const saveImage =async (req,res)=>{
    try{
        let {hinh_id} = req.params;
        let {token} = req.headers
        let userInfo = await getInfoUser(token);
        let dataUser = await prisma.nguoi_dung.findFirst({
            where:{
                nguoi_dung_id :userInfo.nguoi_dung_id
            },
        });
        
        if(dataUser){
            let saveImg ={
                nguoi_dung_id: Number(dataUser.nguoi_dung_id),
                hinh_id : Number(hinh_id),
                ngay_luu: new Date() 
            }
            
            let checkImg =  await prisma.luu_anh.findFirst({
                where:{
                    nguoi_dung_id:saveImg.nguoi_dung_id,
                    hinh_id : saveImg.hinh_id
                }
            })

            if(checkImg){
                failCode(res,checkImg,"This Image already saved");
            }else{
                await prisma.luu_anh.create({data:saveImg});
                successCode(res,saveImg,"Successfully save Image") 
            }
        }else{
            failCode(res,dataUser,"User not found");
        }
        
        
    }catch(err){
        console.log(err);
        failCode(res,"",err.message);
    }
};

const getUser =async(req,res)=>{
    try{
        let {token} = req.headers
        let userInfo = await getInfoUser(token);
        let dataUser = await prisma.nguoi_dung.findFirst({
                where:{
                    nguoi_dung_id :userInfo.nguoi_dung_id
                },
                select:{
                    nguoi_dung_id :true,
                    ho_ten: true,
                    tuoi:true,
                    anh_dai_dien: true,
                    ten_nguoi_dung: true,
                    gioi_thieu:true
                }
                  
        });
        if(dataUser){
            successCode(res,dataUser,"Successfully get user info") 
        }else{
            failCode(res,dataUser,"User not found");
        }
        
        
    }catch(err){
        console.log(err);
        failCode(res,"",err.message);
    }  
}

const getListCreatedImage =async(req,res)=>{
    try{
        let {token} = req.headers
        let userInfo = await getInfoUser(token);
        let dataUser = await prisma.nguoi_dung.findFirst({
                where:{
                    nguoi_dung_id :userInfo.nguoi_dung_id
                },           
        });
        if(dataUser){
            let listImages =  await prisma.hinh_anh.findMany({
                where:{
                    nguoi_dung_id: dataUser.nguoi_dung_id
                },
                select:{
                    hinh_id:true,
                    ten_hinh:true,
                    duong_dan:true,
                    mo_ta:true
                }
            })
            if(listImages.length>0){
                successCode(res,listImages,"Successfully get list Image") 
            }else{
                successCode(res,listImages,"There aren't any Image") 
            }
            
        }else{
            failCode(res,dataUser,"User not found");
        }
        
        
    }catch(err){
        console.log(err);
        failCode(res,"",err.message);
    }  
}

const getListSavedImage =async(req,res)=>{
    try{
        let {token} = req.headers
        let userInfo = await getInfoUser(token);
        let dataUser = await prisma.nguoi_dung.findFirst({
                where:{
                    nguoi_dung_id :userInfo.nguoi_dung_id
                },           
        });
        if(dataUser){
            let listImages =  await prisma.luu_anh.findMany({
                where:{
                    nguoi_dung_id: dataUser.nguoi_dung_id
                },
                include:{
                    hinh_anh:{
                        select:{
                            hinh_id:true,
                            ten_hinh:true,
                            duong_dan:true,
                            mo_ta:true
                        }
                    },
                    
                }
                
            })
            if(listImages.length>0){
                successCode(res,listImages,"Successfully get list Image") 
            }else{
                successCode(res,listImages,"There aren't any Image") 
            }
            
        }else{
            failCode(res,dataUser,"User not found");
        }
        
        
    }catch(err){
        console.log(err);
        failCode(res,"",err.message);
    }  
}

const removeCreatedImage =async(req,res)=>{
    try{
        let {hinh_id} =req.params
        let {token} = req.headers
        let userInfo = await getInfoUser(token);
        let dataUser = await prisma.nguoi_dung.findFirst({
                where:{
                    nguoi_dung_id :userInfo.nguoi_dung_id
                },           
        });

        let dataImage =  await prisma.hinh_anh.findFirst({
            where:{
                hinh_id:Number(hinh_id)
            },
        })
        if(dataImage){
            if(dataUser.nguoi_dung_id === dataImage.nguoi_dung_id){
                await prisma.hinh_anh.delete({
                    where:{
                        hinh_id:Number(hinh_id)
                    }
                })
                successCode(res,hinh_id,"Delete image successfully")
            }else{
                failCode(res,hinh_id,"You dont have permission to delete this Image");
            }
        }else{
            failCode(res,hinh_id,"Image id is not found");
        }
        
        
        
    }catch(err){
        console.log(err);
        failCode(res,"",err.message);
    }  
}

const removeSavedImage =async(req,res)=>{
    try{
        let {hinh_id} =req.params
        let {token} = req.headers
        let userInfo = await getInfoUser(token);
        let dataUser = await prisma.nguoi_dung.findFirst({
                where:{
                    nguoi_dung_id :userInfo.nguoi_dung_id
                },           
        });

        if(dataUser){
            let dataImage = await prisma.luu_anh.findFirst({
                where:{
                    hinh_id:Number(hinh_id),
                    nguoi_dung_id: dataUser.nguoi_dung_id
                }
            })
            console.log(dataImage);
            if(dataImage){
                await prisma.luu_anh.deleteMany({
                    where:{
                        hinh_id:Number(hinh_id),
                        nguoi_dung_id: dataUser.nguoi_dung_id
                    }
                })
                successCode(res,hinh_id,"Delete image successfully")
            }else{
                failCode(res,hinh_id,"User hasn't saved this Image");
            }
            
        }else{
            failCode(res,hinh_id,"User is not found");
        }
        
        
        
    }catch(err){
        console.log(err);
        failCode(res,"",err.message);
    }  
}

const uploadImage =async (req,res)=>{
    //load hinh localhost:8000/public/image/1684636990688_avatar.jpg
    try{
        let file=req.file;
        let {token} = req.headers
        let {mo_ta,duong_dan} = req.body
        let userInfo = await getInfoUser(token);
        let dataUser = await prisma.nguoi_dung.findFirst({
                where:{
                    nguoi_dung_id :userInfo.nguoi_dung_id
                },           
        });
        console.log(file);
        if(dataUser){
            let newImage = {
                ten_hinh:file.filename,
                duong_dan,
                mo_ta,
                nguoi_dung_id : dataUser.nguoi_dung_id
            }
            await prisma.hinh_anh.create({
                data : newImage
            })
            successCode(res,newImage,"Upload Image successfully")
        }else{
            failCode(res,dataUser,"User is not found");
        }
        
        
        
    }catch(err){
        console.log(err);
        failCode(res,"",err.message);
    }  

};

const updateProfile =async (req,res)=>{
    //load hinh localhost:8000/public/avatar/1684636990688_avatar.jpg
    try{
        let file=req.file;
        let {token} = req.headers
        let {ho_ten,tuoi,ten_nguoi_dung,gioi_thieu} = req.body
        let userInfo = await getInfoUser(token);
        let dataUser = await prisma.nguoi_dung.findFirst({
                where:{
                    nguoi_dung_id :userInfo.nguoi_dung_id
                },           
        });
        console.log(file);
        if(dataUser){
            let newProfile = {
                ho_ten,
                tuoi: Number(tuoi),
                anh_dai_dien:file.filename,
                ten_nguoi_dung,
                gioi_thieu
            }
            await prisma.nguoi_dung.update({
                data : newProfile,
                where :{
                    nguoi_dung_id:dataUser.nguoi_dung_id
                }
            })
            successCode(res,newProfile,"Update Profile successfully")
        }else{
            failCode(res,dataUser,"User is not found");
        }
         
    }catch(err){
        console.log(err);
        failCode(res,"",err.message);
    }  

};

const getInfoUser = async (token) => {
    let infoUser = decodeToken(token);
    console.log('infoUser',infoUser)
    let getUser = await prisma.nguoi_dung.findFirst({
        where: {
            nguoi_dung_id: infoUser.data.nguoi_dung_id
        }
    })
    // thông tin user id sẽ lấy từ token
    console.log('infoUser getUser',getUser)
    return getUser;
}

export{
    userLogin,
    userSignUp,
    addComment,
    saveImage,
    uploadImage,
    getUser,
    getInfoUser,
    getListCreatedImage,
    getListSavedImage,
    removeCreatedImage,
    removeSavedImage,
    updateProfile
}