import {successCode,failCode,errorCode} from "../config/Response.js";
import { PrismaClient } from "@prisma/client";
import {generateToken, checkToken, decodeToken} from "../config/jwt.js"
import { getInfoUser } from "./userController.js";

const prisma = new PrismaClient;

const getImg = async(req,res)=>{
    try{
        let data = await prisma.hinh_anh.findMany({
            include:{
                nguoi_dung:{
                    select:{
                        ho_ten:true
                    }
                }
            }
        });
        if(data){
            successCode(res,data,"Get all images successful")
        }else{
            failCode(res,data,"There are no images to display")
        }
        
    }catch(err){
        console.log(err);
        failCode(res,"","Cant get Image");
    }
 
}

const getImgByName = async(req,res)=>{
    try{
        let {ten_hinh} = req.params;
        let listImages = await prisma.hinh_anh.findMany({
            where:{
                ten_hinh: {
                    contains: ten_hinh,
                }
            },
            include:{
                nguoi_dung:{
                    select:{
                        ho_ten:true
                    }
                }
            }
        });
        if(listImages.length>0){
            successCode(res,listImages,"Get images successful")
        }else{
            failCode(res,listImages,"There are no images to display")
        }
    }catch(err){
        console.log(err);
        failCode(res,"","Cant get Image");
    }
 
}

const getImgInfoByID = async(req,res)=>{
    try{
        let {hinh_id} = req.params;
        let dataImage = await prisma.hinh_anh.findFirst({
            where:{
                hinh_id:Number(hinh_id)
            },
            include:{
                nguoi_dung:{
                    select:{
                        ho_ten:true
                    }
                }
            }
        });
        if(dataImage){
            successCode(res,dataImage,"Get images successful")
        }else{
            failCode(res,dataImage,"There are no images to display")
        }
    }catch(err){
        console.log(err);
        failCode(res,"","Cant get Image");
    }
 
}

const getImgCommentByID = async(req,res)=>{
    try{
        let {hinh_id} = req.params;
        let dataImage = await prisma.binh_luan.findMany({
            where:{
                hinh_id:Number(hinh_id)
            },
            include:{
                nguoi_dung:{
                    select:{
                        ho_ten:true
                    }
                }
            }
        });
        dataImage = dataImage.map(img =>{
            let listImages ={};
            listImages.ngay_binh_luan = img.ngay_binh_luan;
            listImages.nguoi_dung = img.nguoi_dung.ho_ten;
            listImages.noi_dung = img.noi_dung;
            return listImages
        })
        successCode(res,dataImage,"")
    }catch(err){
        console.log(err);
        failCode(res,"","Cant get Comment");
    }
 
}

const checkSavedImgByID = async(req,res)=>{
    try{
        let {hinh_id} = req.params;
        let {token} = req.headers
        let userInfo = await getInfoUser(token);
        let dataImage = await prisma.luu_anh.findFirst({
            where:{
                hinh_id:Number(hinh_id),
                nguoi_dung_id :userInfo.nguoi_dung_id
            },
        });
        let status = {isSaved : false};
        if(dataImage){
            status.isSaved = true;
            successCode(res,status,"Saved")
        }else{
            successCode(res,status,"Not Saved")
        }
        
    }catch(err){
        console.log(err);
        failCode(res,"",err.message);
    }
 
}



export {
    getImg,
    getImgByName,
    getImgInfoByID,
    getImgCommentByID,
    checkSavedImgByID
}