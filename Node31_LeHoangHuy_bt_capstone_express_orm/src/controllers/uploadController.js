
//yarn add multer
import multer from "multer";

const storage = multer.diskStorage({
    destination: process.cwd() +"/public/image",
    filename : (req, file, callback)=>{
        let newName = new Date().getTime()+"_"+file.originalname.replace(/[^a-zA-Z0-9. ]/g, '');
        callback(null,newName);
    }
})

const storageAvatar = multer.diskStorage({
    destination: process.cwd() +"/public/avatar",
    filename : (req, file, callback)=>{
        let newName = new Date().getTime()+"_"+file.originalname.replace(/[^a-zA-Z0-9. ]/g, '');
        callback(null,newName);
    }
})
const upload = multer({storage:storage}); //destination => duong dan de luu tru tai nguyen
const uploadAvatar = multer({storage:storageAvatar}); //destination => duong dan de luu tru tai nguyen

export {
    upload,
    uploadAvatar
} 