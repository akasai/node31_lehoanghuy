-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: db_node31_mypicture
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `binh_luan`
--

DROP TABLE IF EXISTS `binh_luan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `binh_luan` (
  `binh_luan_id` int NOT NULL AUTO_INCREMENT,
  `nguoi_dung_id` int DEFAULT NULL,
  `hinh_id` int DEFAULT NULL,
  `ngay_binh_luan` datetime DEFAULT NULL,
  `noi_dung` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`binh_luan_id`),
  KEY `nguoi_dung_id` (`nguoi_dung_id`),
  KEY `binh_luan_ibfk_2` (`hinh_id`),
  CONSTRAINT `binh_luan_ibfk_1` FOREIGN KEY (`nguoi_dung_id`) REFERENCES `nguoi_dung` (`nguoi_dung_id`),
  CONSTRAINT `binh_luan_ibfk_2` FOREIGN KEY (`hinh_id`) REFERENCES `hinh_anh` (`hinh_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `binh_luan`
--

LOCK TABLES `binh_luan` WRITE;
/*!40000 ALTER TABLE `binh_luan` DISABLE KEYS */;
INSERT INTO `binh_luan` VALUES (1,1,1,'2023-06-01 09:00:00','hinh tui up do'),(2,2,1,'2023-06-02 13:00:00','hinh dep lam'),(3,1,1,'2023-06-05 19:00:00','thank ban'),(4,2,1,'2023-06-28 07:40:28','1234'),(5,2,1,'2023-06-28 07:50:16','test test'),(10,8,1,'2023-07-01 10:28:09','this picture is beautiful');
/*!40000 ALTER TABLE `binh_luan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hinh_anh`
--

DROP TABLE IF EXISTS `hinh_anh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hinh_anh` (
  `hinh_id` int NOT NULL AUTO_INCREMENT,
  `ten_hinh` varchar(100) DEFAULT NULL,
  `duong_dan` varchar(250) DEFAULT NULL,
  `mo_ta` varchar(255) DEFAULT NULL,
  `nguoi_dung_id` int DEFAULT NULL,
  PRIMARY KEY (`hinh_id`),
  KEY `nguoi_dung_id` (`nguoi_dung_id`),
  CONSTRAINT `hinh_anh_ibfk_1` FOREIGN KEY (`nguoi_dung_id`) REFERENCES `nguoi_dung` (`nguoi_dung_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hinh_anh`
--

LOCK TABLES `hinh_anh` WRITE;
/*!40000 ALTER TABLE `hinh_anh` DISABLE KEYS */;
INSERT INTO `hinh_anh` VALUES (1,'picture1.js','http://localhost:8080/public/image','day la hinh 1',1),(2,'picture2.js','http://localhost:8080/public/image','day la hinh 2',2),(3,'avatar.jpg','http://localhost:8080/public/image','day la avatar',1),(6,'1687957599019_drowranger.jpg','http://localhost:8000/public/image','drow ranger in dota 2 by ai',2),(7,'1688037978731_master.jpg','http://localhost:8000/public/image','my master avatar',3),(8,'1688192554356_nguongocjuggernautdota2.jpg','http://localhost:8000/public/image','im juggernaut',4),(9,'1688192810161_Juggernaut.jpg','https://s1.1zoom.me/b5340/Juggernaut_DOTA_2_Masks_Warriors_514099_300x169.jpg','im juggernaut',4);
/*!40000 ALTER TABLE `hinh_anh` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `luu_anh`
--

DROP TABLE IF EXISTS `luu_anh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `luu_anh` (
  `nguoi_dung_id` int NOT NULL,
  `hinh_id` int NOT NULL,
  `ngay_luu` datetime DEFAULT NULL,
  PRIMARY KEY (`nguoi_dung_id`,`hinh_id`),
  KEY `luu_anh_ibfk_2` (`hinh_id`),
  CONSTRAINT `luu_anh_ibfk_1` FOREIGN KEY (`nguoi_dung_id`) REFERENCES `nguoi_dung` (`nguoi_dung_id`),
  CONSTRAINT `luu_anh_ibfk_2` FOREIGN KEY (`hinh_id`) REFERENCES `hinh_anh` (`hinh_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `luu_anh`
--

LOCK TABLES `luu_anh` WRITE;
/*!40000 ALTER TABLE `luu_anh` DISABLE KEYS */;
INSERT INTO `luu_anh` VALUES (1,1,'2023-06-28 08:15:34'),(1,2,'2023-06-28 08:15:34'),(2,1,'2023-06-28 08:15:34'),(4,6,'2023-07-01 06:16:41'),(8,2,'2023-07-01 10:37:07');
/*!40000 ALTER TABLE `luu_anh` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nguoi_dung`
--

DROP TABLE IF EXISTS `nguoi_dung`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `nguoi_dung` (
  `nguoi_dung_id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(50) DEFAULT NULL,
  `mat_khau` varchar(250) DEFAULT NULL,
  `ho_ten` varchar(50) DEFAULT NULL,
  `tuoi` int DEFAULT NULL,
  `anh_dai_dien` varchar(250) DEFAULT NULL,
  `ten_nguoi_dung` varchar(50) DEFAULT NULL,
  `gioi_thieu` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`nguoi_dung_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nguoi_dung`
--

LOCK TABLES `nguoi_dung` WRITE;
/*!40000 ALTER TABLE `nguoi_dung` DISABLE KEYS */;
INSERT INTO `nguoi_dung` VALUES (1,'huytest@gmail.com','$2b$10$6rNdD/RBSc2iqBw90YE3UepnzhexoFvArzxrrWn0T8EqrIGq4zINe','Le Hoang Huy',30,NULL,NULL,NULL),(2,'phantom_assasin@gmail.com','$2b$10$B6Cy2j8Z6ZtUE68GSxf.y.1xMwZosCJcJRUWWKvACHSMABaRlN8sO','Phantom Assasin',18,NULL,NULL,NULL),(3,'wind_ranger@gmail.com','$2b$10$JcwQVXuorpSt93PyvNwJIOVnQ63wtXNbPLVfUN/eYa23rWHrN.t0q','Lyralei',25,'1688037845252_winddranger.jpg','Wind Ranger','Master archer of the wood, and favored godchild of the wind.'),(4,'juggernautr@gmail.com','$2b$10$xMO.TnR95SjkwthR4ssfi.nB6qHx5nB2MaZ0aJOnwuQzg2XVgCvzS','Yurnero ',30,'1688193018292_Juggernaut.jpg','Juggurnaut','By the Visage of Vengeance, which drowned in the Isle of Masks, I will carry on the rites of the Faceless Ones'),(8,'marcy@gmail.com','$2b$10$p.quQj8Tet.eL92/gpwq4.hyned6Pjb4UiYpOpSSah7NoSLSRUNIy','Marci ',30,'1688207625791_marci.jpg','Maid Marci','Im a hero in Dota 2. Im a support');
/*!40000 ALTER TABLE `nguoi_dung` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-01 18:10:53
